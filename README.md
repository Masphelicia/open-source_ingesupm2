# Post-installation setup script for CentOS 7 servers 

(c) Niki Kovacs, 2020

Ce répertoire donne accès à un script de setup post-installation "automagique'
Pour les serveurs tournant sous CentOS7, ainsi qu'à une collection de scripts
d'aide et de template de fichiers de configuration pour les services communs.

## In a nutshell

Effectuer les étapes suivantes.

  1. Installer une machine CentOS 7 minimale.

  2. Créer un compte non-root avec des privilèges administrateurs.

  3. Installer Git: `sudo yum install git`

  4. Prendre le script: git clone https://gitlab.com/kikinovak/centos-7.git`

  5. Aller dans le nouveau répertoire: `cd centos-7`

  6. Lancer le script: `sudo ./centos-setup.sh --setup`

  7. Attraper une tasse de café pendant que le script fait tout le travail.

  8. Reboot.


## Customizing a CentOS server

Trasformer un serveur CentOS minimal en un serveur fonctionnel se résume
toujours à une suite d'opérations plus ou moins chronophage. Votre kilométrage?
Peut varier bien entendu, mais voila ce que je fais d'habitude sur une
installation fraîche de CentOS:

  * Personaliser le Bash Shell : prompt, aliases, etc.

  * Personaliser l'éditeur Vim.

  * Installer les dépots tiers officiels.

  * Installer un ensemble d'outils de ligne de commande.

  * Supprimer quelques pacquets inutiles.

  * Permettre à l'utilisateur admin l'accès aux logs du système.

  * Désactiver l'IPv6 et reconfigurer quelques services en conséquence.
  
  * Configurer un mot de passe persistent pour `sudo`.

  * Etc.

Le script `centos-setup.sh` réalise toutes ces opérations.

Configurer Bash et Vim et mise en place d'une résolution de console par défaut
plus lisible:

```
# ./centos-setup.sh --shell
```

Mise en place des dépots tiers officiels:

```
# ./centos-setup.sh --repos
```

Installer les groupes de paquets `Core` et `Base` ainsi que quelques outils supplémentaires:

```
# ./centos-setup.sh --extra
```

Supprimer quelques paquets inutiles:

```
# ./centos-setup.sh --prune
```

Permettre à l'utilisateur admin d'accéder au logs du système:

```
# ./centos-setup.sh --logs
```

Désactiver l'IPv6 et reconfigurer les services basiques en conséquence:

```
# ./centos-setup.sh --ipv4
```

Configurer un mot de passe persistent pour sudo:

```
# ./centos-setup.sh --sudo
```

Effectuer tous les points précédents en une fois:

```
# ./centos-setup.sh --setup
```

Dépiler les paquets et revenir à une version améliorée du système de base:

```
# ./centos-setup.sh --strip
```

Afficher le message d'aide:

```
# ./centos-setup.sh --help
```

Si vous souhaitez savoir exactement de qu'il se passe sous le capôt, ouvrir un
second terminal et regarder les logs:

```
$ tail -f /tmp/centos-setup.log
```
